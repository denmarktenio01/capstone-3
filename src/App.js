import './App.css';
import React, {Fragment, useState} from 'react'
import {Container} from 'react-bootstrap'
import {
  BrowserRouter as Router
} from 'react-router-dom'
import { Route, Switch } from 'react-router';


import UserContext from './UserContext';




// pages
import Welcome from './components/Welcome';
import Home from './pages/Home';
import NavBar from './components/NavBar';
import NotFound from './pages/NotFound';
import Register from './pages/Register';
import Login from './pages/Login';
import Products from './pages/Products';
import ProductsAdmin from './pages/ProductsAdmin';
import Orders from './pages/Orders';
import OrdersAdmin from './pages/OrdersAdmin';
import UserAccount from './pages/UserAccount';

import NotAdmin from './pages/NotAdmin';










function App() {

  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true',
    firstName: localStorage.getItem('firstName'),
    lastName: localStorage.getItem('lastName'),
    email: localStorage.getItem('email'),
    mobileNumber: localStorage.getItem('mobileNumber')
  });
  
  const unsetUser = () => {
    localStorage.clear()
    setUser({
      accessToken: null,
      isAdmin: null,
      firstName: null,
      lastName: null,
      email: null,
      mobileNumber: null
      })
  } 










  return (


    
    <UserContext.Provider value={{user, setUser, unsetUser}}>
      
      <Router>
        <NavBar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home}/>
            {user.isAdmin ? <Route exact path="/productsAdmin" component={ProductsAdmin} /> : <Route exact path="/productsAdmin" component={NotAdmin} />}
            <Route exact path="/shop" component={Products} />
            

            {/* ORDER ROUTES */}
            <Route exact path="/myOrders" component={Orders} />
            {user.isAdmin ? <Route exact path="/adminOrders" component={OrdersAdmin} /> : <Route exact path="/adminOrders" component={NotAdmin} />}
            



          {/* USER INFORMATION */}
          <Route exact path="/userInfo" component={UserAccount} />



            <Route exact path="/register" component={Register}/>
            <Route exact path="/login" component={Login} />


            {/* INVALID ROUTES HANDLING */}
            <Route exact path="/naughty" component={NotAdmin} />
            <Route component={NotFound}/>
            
          </Switch>
        </Container>
      </Router>
    </UserContext.Provider>
  );
}

export default App;
