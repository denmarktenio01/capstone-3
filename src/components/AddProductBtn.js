import React, { useContext, useEffect, useState } from "react"
import { Fragment } from "react"
import {Modal, Button, Form} from "react-bootstrap"
import Swal from "sweetalert2"
import UserContext from "../UserContext"

export default function AddProductBtn() {
    const {user, setUser} = useContext(UserContext)
    const [show, setShow] = useState(false)
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const [imageSrc, setImageSrc] = useState("")
    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [price, setPrice] = useState(0)
    const [isActive, setIsActive] = useState(false)


    useEffect(() => {
        if (name !== "" && description !== "" && price !== 0) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, description, price])


    function addProduct() {
        fetch('https://quiet-waters-16548.herokuapp.com/products/add', {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.accessToken}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data._id !== undefined) {
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: `Product added!`
                })
                .then(() => window.location.reload())
            } else {
                Swal.fire({
                    title: 'Uh oh!',
                    icon: 'error',
                    text: `${data.message}`
                })
            }
        })
    }













    return (
        <div className='text-center'>

        <Button variant='success' onClick={handleShow}>Add Product</Button>

        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
            <Form.Group>
                    <Form.Label>Image:</Form.Label>
                    <Form.Control 
                    type='text'
                    value={imageSrc}
                    placeholder='Enter image link'
                    onChange={e => setImageSrc(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Name:</Form.Label>
                    <Form.Control 
                    type='text'
                    value={name}
                    placeholder='Enter product name'
                    onChange={e => setName(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Description:</Form.Label>
                    <Form.Control 
                    value={description}
                    type='text'
                    placeholder='Enter product description'
                    onChange={e => setDescription(e.target.value)}
                    required
                    ></Form.Control> 
                </Form.Group>
                <Form.Group>
                    <Form.Label>Price:</Form.Label>
                    <Form.Control 
                    type='number'
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" disabled={!isActive} onClick={addProduct}>
            Save
          </Button>
        </Modal.Footer>
      </Modal>

      </div>
    )







}