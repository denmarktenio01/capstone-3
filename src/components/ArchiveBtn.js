import Swal from 'sweetalert2'
import React, { useContext } from 'react'
import { Button } from 'react-bootstrap'
import UserContext from '../UserContext'

export default function ArchiveBtn({productId}) {
    const {user,setUser} = useContext(UserContext)

    function archiveProduct(e) {
        e.preventDefault()

        fetch(`https://quiet-waters-16548.herokuapp.com/products/${productId}/archive`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${user.accessToken}`,
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data) {  
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: `Product archived!`
                })
                .then(function(){window.location.reload()})
                
            } else {
                Swal.fire({
                    title: 'Uh oh!',
                    icon: 'error',
                    text: `Something went wrong!`
                })
            }
        }
        
        )
    }


    return (
        
        <Button variant='danger' onClick={archiveProduct}>Archive</Button>
    )
}