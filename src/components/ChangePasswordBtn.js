import React, { useContext, useEffect, useState } from "react"
import {Modal, Button, Form} from "react-bootstrap"
import Swal from "sweetalert2"
import UserContext from "../UserContext"

export default function ChangePasswordBtn() {
    const {user} = useContext(UserContext)
    const [show, setShow] = useState(false)
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)
    const [isActive, setIsActive] = useState(false)


    const [currentPassword, setCurrentPassword] = useState("")
    const [newPassword, setNewPassword] = useState("")
    const [verifyPassword, setVerifyPassword] = useState("")



    useEffect(() => {
        if (currentPassword !== "" && newPassword !== "" && verifyPassword !== "") {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [currentPassword, newPassword, verifyPassword])



    function changePass() {
        fetch('https://quiet-waters-16548.herokuapp.com/users/changePassword', {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.accessToken}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                currentPassword: currentPassword,
                newPassword: newPassword,
                verifyPassword: verifyPassword
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true){
                Swal.fire({
                    title: 'Done!',
                    icon: 'success',
                    text: 'Password changed!'
                })
            } else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: `Something went wrong.`
                })
            }
        })
        setCurrentPassword("")
        setNewPassword("")
        setVerifyPassword("")
    }




    return (
        <>

        <Button variant='primary' className='ml-5' onClick={handleShow}>Change Password</Button>

        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Change Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label>Current Password:</Form.Label>
                    <Form.Control 
                    type='password'
                    value={currentPassword}
                    placeholder='Enter your current password'
                    onChange={e => setCurrentPassword(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>New Password:</Form.Label>
                    <Form.Control 
                    value={newPassword}
                    type='password'
                    placeholder='Enter new password'
                    onChange={e => setNewPassword(e.target.value)}
                    required
                    ></Form.Control> 
                </Form.Group>
                {/* <Form.Group>
                    <Form.Label>Email:</Form.Label>
                    <Form.Control 
                    type='email'
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group> */}
                <Form.Group>
                    <Form.Label>Verify New Password:</Form.Label>
                    <Form.Control 
                    type='password'
                    value={verifyPassword}
                    onChange={e => setVerifyPassword(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" disabled={!isActive} onClick={changePass}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>

      </>
    )














} 