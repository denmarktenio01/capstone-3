import React, { useContext, useEffect, useState } from "react"
import {Modal, Button, Form} from "react-bootstrap"
import Swal from "sweetalert2"

export default function EditInfo() {
    const [show, setShow] = useState(false)
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)
    const [isActive, setIsActive] = useState(false)


    const [firstName, setFirstName] = useState(localStorage.firstName)
    const [lastName, setLastName] = useState(localStorage.lastName)
    const [mobileNumber, setMobileNumber] = useState(localStorage.mobileNumber)



    useEffect(() => {
        if (firstName !== "" && lastName !== "" && mobileNumber.length === 11) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [firstName, lastName, mobileNumber])



    function updateInfo() {
        fetch('https://quiet-waters-16548.herokuapp.com/users/editDetails', {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.accessToken}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                mobileNumber: mobileNumber
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data._id !== undefined) {
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: `Your account has been updated!`
                })
                .then(() => {
                    localStorage.setItem('isAdmin',data.isAdmin)   
                    localStorage.setItem('firstName',data.firstName)
                    localStorage.setItem('lastName',data.lastName)
                    localStorage.setItem('mobileNumber',data.mobileNumber)
                    window.location.reload()
                })
            } else {
                Swal.fire({
                    title: 'Uh oh!',
                    icon: 'error',
                    text: `${data.message}`
                })
            }
        })
    }




    return (
        <>

        <Button variant='primary' onClick={handleShow}>Edit</Button>

        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Account</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <Form.Group>
                    <Form.Label> First Name:</Form.Label>
                    <Form.Control 
                    type='text'
                    value={firstName}
                    placeholder='Enter new product name'
                    onChange={e => setFirstName(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Last Name:</Form.Label>
                    <Form.Control 
                    value={lastName}
                    type='text'
                    placeholder='Enter new product description'
                    onChange={e => setLastName(e.target.value)}
                    required
                    ></Form.Control> 
                </Form.Group>
                {/* <Form.Group>
                    <Form.Label>Email:</Form.Label>
                    <Form.Control 
                    type='email'
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group> */}
                <Form.Group>
                    <Form.Label>Mobile Number:</Form.Label>
                    <Form.Control 
                    type='number'
                    value={mobileNumber}
                    onChange={e => setMobileNumber(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" disabled={!isActive} onClick={updateInfo}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>

      </>
    )














} 