import React, {Fragment, useContext, useState} from 'react'
// import necessary components
import Navbar from 'react-bootstrap/Navbar'; // export default = it can be changed or placed
// on any variable we want  
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function NavBar() {
    const history = useHistory()
    const {user, setUser, unsetUser} = useContext(UserContext)

    

    const logout = () => {
        unsetUser()
        setUser({accessToken: null})
        history.push('/login')
    }
    // For User
    let orderNav = (!user.isAdmin && user.accessToken !== null) ? <Nav.Link as={NavLink} to="/myOrders">My Orders</Nav.Link> : <></>

    let shopLink = (!user.isAdmin) ? <Nav.Link as={NavLink} to="/shop">Shop</Nav.Link> : <></>


    // For ADMIN
    let adminOrderNav = (user.isAdmin && user.accessToken !== null) ?
    <Nav.Link as={NavLink} to="/adminOrders">Orders</Nav.Link>
    :
    <></>

    let productLink = (user.isAdmin && user.accessToken !== null) ? <Nav.Link as={NavLink} to="/productsAdmin">Products Dashboard</Nav.Link> : <></>

    
    
    




    let rightNav = (user.accessToken !== null) ? 
    <Fragment>
        <Nav.Link as={NavLink} to="/userInfo">Account</Nav.Link>
        <Nav.Link onClick={logout}>Logout</Nav.Link>
    </Fragment>

    :

    <Fragment>
        <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
        <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
    </Fragment>


    return (
        <Navbar bg='dark' variant='dark' expand='lg' fixed='top'>
            <Navbar.Brand className='brand-name' as={Link} to="/">OMNI</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse className='text-center' id='basic-navbar-nav'>
                {user.isAdmin 
                    ?
                    <Nav className='mr-auto'>
                        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                        {productLink}
                        {adminOrderNav}
                    </Nav>
                    :
                    <Nav className='mr-auto'>
                        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                        {shopLink}
                        {orderNav}
                    </Nav>
                }
                

                <Nav className='ml-auto'>
                    {rightNav}
                </Nav>




            </Navbar.Collapse>




        </Navbar>
    )



    






}