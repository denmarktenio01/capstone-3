import React, { useContext, useEffect, useState } from 'react'
import { Button, Table, Modal, Form } from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import { useHistory } from 'react-router-dom'
import Product from './Product'
import { Fragment } from 'react'

export default function Order({products, productId}) {
    const [isActive, setIsActive] = useState(false)

    const {
        name, description, price
    } = products
    const [quantity, setQuantity] = useState(0)
    const [show, setShow] = useState(false);

    const handleClose = () => {
        setQuantity(0)
        setShow(false)

    
    };
    const handleShow = () => setShow(true);

    useEffect(() => {
        if(quantity !== 0) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [quantity])


    function order (productId) {
        fetch(`https://quiet-waters-16548.herokuapp.com/users/order`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.accessToken}`
            },
            body: JSON.stringify({
                productId: productId,
                productName: name,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data) {
                Swal.fire({
                    title: 'Done!',
                    icon: 'success',
                    text: `Order sucessfuly placed!`
                })
            } else {
                Swal.fire({
                    title: 'Oops!',
                    icon: 'error',
                    text: `There was a problem processing your order.`
                })
            }
        })
    }










    return (
        <>
          <Button variant="primary" onClick={handleShow}>
            Order
          </Button>
    
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>{name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h3 className='text-center'>How many would you like to order?</h3>
                <Form>
                    <Form.Group>
                        <Form.Label>Quantity:</Form.Label>
                        <Form.Control 
                        type='number'
                        value={quantity}
                        onChange={e => setQuantity(e.target.value)}
                        required
                        ></Form.Control>
                    </Form.Group>
                </Form>
                <h3 className='mr-auto'>
                    Total Price: {price * quantity}
                </h3>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" disabled={!isActive} onClick={order()}>
                Confirm Order
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      );










}