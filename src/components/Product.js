import React, {useState, useEffect, useContext} from 'react'
import {
    Row, Col, Card, Button, Form, Modal
} from 'react-bootstrap'
import Swal from 'sweetalert2';
// Add PropTypes in the Course component to validate its props
import PropTypes from 'prop-types'
import ProductDetails from './ProductDetails';
import Order from './Order';
import { useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
// apply the PropTypes in the Course Component to make sure that the component is getting the correct property types
// 

export default function Product({products, productId}) {
    const {user} = useContext(UserContext)
    const history = useHistory()
    const {
        name, description, price, imageSrc
    } = products

    const [quantity, setQuantity] = useState(0)
    const [isActive, setIsActive] = useState(false)

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    useEffect(() => {
        if(quantity !== 0 && quantity.length !== 0) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }   
    }, [quantity])



    // FUNCTION FOR PLACING AN ORDER
    function order (productId) {
        fetch(`https://quiet-waters-16548.herokuapp.com/users/order`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.accessToken}`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true) {
                Swal.fire({
                    title: 'Done!',
                    icon: 'success',
                    text: `Order sucessfuly placed!`
                }).then(() => handleClose())
            } else {
                Swal.fire({
                    title: 'Oops!',
                    icon: 'error',
                    text: `There was a problem processing your order.`
                }).then(() => handleClose())
            }
        })
    }


    // FUNTION FOR WHEN THERE IS NO ACCOUNT LOGGED IN
    function loginFirst() {
        Swal.fire({
            title: 'Oops!',
            icon: 'info',
            text: `You have to login to order.`
        }).then(() => history.push('/login'))
    }






    


    return (
        <Col className="col-8 my-3"  style={{ width: '100%' }}>
            <Card bg='dark' text='light' border='warning'>
                <Card.Img variant="top" src={imageSrc} />
                <Card.Header>
                    <h3 className='text-center'>{name}</h3>
                </Card.Header>
                <Card.Body>

                    <Card.Text>
                        <h5 className='text-center'>
                            Php {price}
                        </h5>
                    </Card.Text>
                    <Card.Footer className='d-flex justify-content-around'>
                    {user.accessToken !== null ?
                        <Button variant="primary" onClick={handleShow}>
                            Order
                        </Button>
                        :
                        <Button variant="primary" onClick={() => loginFirst()}>
                            Order
                        </Button>
                    }
                    

                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>
                        <Modal.Title>{name}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <h3>How many would you like to order?</h3>
                            <Form>
                                <Form.Group>
                                    <Form.Label>Quantity:</Form.Label>
                                    <Form.Control 
                                    type='number'
                                    value={quantity}
                                    onChange={e => setQuantity(e.target.value)}
                                    required
                                    ></Form.Control>
                                </Form.Group>
                            </Form>
                            <h3 className='mr-auto'>
                                Total Price: {price * quantity}
                            </h3>
                        </Modal.Body>
                        <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" disabled={!isActive} onClick={() => order(productId)}>
                            Confirm Order
                        </Button>
                        </Modal.Footer>
                    </Modal>
                        <ProductDetails products={products} />
                    </Card.Footer>
                </Card.Body>
            </Card>
        </Col>
        
    )












    






}

Product.PropType = {
    // call on the property to check the data
    //shape() is used to check that a prop object conforms to a specific "shape"
    products: PropTypes.shape({
        // define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}