import React, { useContext, useEffect, useState } from 'react'
import { Button, Table, Modal } from 'react-bootstrap'

import UserContext from '../UserContext'
import { useHistory } from 'react-router-dom'
import Product from './Product'
import { Fragment } from 'react'
import Order from './Order'

export default function ProductDetails({products}) {
    const {
        name, description, price
    } = products
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);




    

    return (
        <>
        <Button variant="info" onClick={handleShow}>
            About this game...
        </Button>

        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
            <Modal.Title>{name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h6>Description:</h6>
                <p>{description}</p>
                <h6>Price:</h6>
                <p>Php {price}</p>
            </Modal.Body>
            <Modal.Footer>
            
            <Button variant="secondary" onClick={handleClose}>
                Close
            </Button>

            </Modal.Footer>
        </Modal>
        </>
    );
  


    


}