import Swal from 'sweetalert2'
import React, { useContext } from 'react'
import { Button } from 'react-bootstrap'
import UserContext from '../UserContext'

export default function RemoveOrderBtn({orderId}) {
    const {user,setUser} = useContext(UserContext)


    function removeOrder(e) {   
        e.preventDefault() 
        fetch('https://quiet-waters-16548.herokuapp.com/users/myOrders/remove', {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.accessToken}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                orderId: orderId
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data._id !== null){
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Order cancelled."
                }).then(() => window.location.reload())
            } else {
                Swal.fire({
                    title: "Uh oh!",
                    icon: "error",
                    text: "Something went wrong."
                })
            }
        })
    }



    return (
        <Button variant='danger' onClick={removeOrder}>Cancel</Button>
    )
}