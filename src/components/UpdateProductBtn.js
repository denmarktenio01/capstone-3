import React, { useContext, useEffect, useState } from "react"
import {Modal, Button, Form} from "react-bootstrap"
import Swal from "sweetalert2"
import UserContext from "../UserContext"

export default function UpdateProductBtn({productId}) {
    const {user, setUser} = useContext(UserContext)
    const [show, setShow] = useState(false)
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)


    const [imageSrc, setImageSrc] = useState("")
    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [price, setPrice] = useState(0)
    const [isActive, setIsActive] = useState(false)


    useEffect(() => {
        if (imageSrc !== "" && name !== "" && description !== "" && price !== 0) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, description, price])


    function updateProduct() {
        fetch(`https://quiet-waters-16548.herokuapp.com/products/${productId}/update`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.accessToken}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                imageSrc: imageSrc,
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            // if(data.message !== undefined) {
            //     Swal.fire({
            //         title: 'Uh oh!',
            //         icon: 'error',
            //         text: `${data.message}`
            //     })
            // }
            if(data._id !== undefined) {
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: `Product updated!`
                })
                .then(() => window.location.reload())
            } else {
                Swal.fire({
                    title: 'Uh oh!',
                    icon: 'error',
                    text: `Something went wrong.`
                })
            }
        })
    }













    return (
        <>

        <Button variant='primary' onClick={handleShow}>Update Product</Button>

        <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
            <Form.Group>
                    <Form.Label>Image:</Form.Label>
                    <Form.Control 
                    type='text'
                    value={imageSrc}
                    placeholder='Enter new image link'
                    onChange={e => setImageSrc(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Name:</Form.Label>
                    <Form.Control 
                    type='text'
                    value={name}
                    placeholder='Enter new product name'
                    onChange={e => setName(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Description:</Form.Label>
                    <Form.Control 
                    value={description}
                    type='text'
                    placeholder='Enter new product description'
                    onChange={e => setDescription(e.target.value)}
                    required
                    ></Form.Control> 
                </Form.Group>
                <Form.Group>
                    <Form.Label>Price:</Form.Label>
                    <Form.Control 
                    type='number'
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required
                    ></Form.Control>
                </Form.Group>
            </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" disabled={!isActive} onClick={updateProduct}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>

      </>
    )







}