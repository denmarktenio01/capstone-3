import React, { useContext, useEffect, useState } from 'react'
import { Button, Table, Card, CardDeck, Col, Row, Container, Collapse } from 'react-bootstrap'
import UserContext from '../UserContext'


export default function ViewOrders({user}) {
    const {
        name, _id, orders
    } = user

    const [userOrders, setUserOrders] = useState([])
    const [overallPrice,setOverallPrice] = useState(orders.map(order => order.totalAmount.totalPrice).reduce((prev,next) => prev+next))

    // COLLAPSE CONTROL
    const [open, setOpen] = useState(false);
    // const toggle = () => setIsOpen(!isOpen);

    // data.map(order => order.totalAmount.totalPrice).reduce((prev,next) => prev+next)

    useEffect(() => {
        setUserOrders(orders.map(order => {
                
            return (
                <tr key={order._id}>
                        <td className='text-center'>{order.purchasedOn}</td>
                        <td className='text-center'>{order.productName}</td>
                        <td className='text-center'>{order.totalAmount.quantity}</td>
                        <td className='text-center'>{order.totalAmount.totalPrice}</td>
                        {/* <td className='d-flex justify-content-around'>
                        </td> */}
                </tr>
            )
        }))
    }, [])



    return (
        <>
            
            <Button
                onClick={() => setOpen(!open)}
                aria-controls="example-collapse-text"
                aria-expanded={open}
            >
                Orders
            </Button>
            <Collapse in={open} className='text-light'>
                <Table className='text-center mt-4' striped bordered hover>
                    <thead>
                        <tr>
                            <th className='text-center'>Date Ordered</th>
                            <th className='text-center'>Ordered Product</th>
                            <th className='text-center'>Quantity</th>
                            <th className='text-center'>Price</th>
                        </tr>
                    </thead>
                    <tbody>{userOrders}</tbody>
                    <tfoot>
                        <tr>
                            <th className='text-center'></th>
                            <th className='text-center'></th>
                            <th className='text-center'></th>
                            <th className='text-center'>Total bill: Php {overallPrice}</th>
                        </tr>
                    </tfoot>
                </Table>
            </Collapse>
            
        </>
    )
}