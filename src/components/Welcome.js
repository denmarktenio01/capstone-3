import React from 'react'
import {
    Jumbotron,
    Button,
    Row,
    Col
} from 'react-bootstrap'


export default function Welcome () {

    return (
                <Jumbotron className='brand-jumbotron fluid mt-5'>
                    <h1 className='text-center brand-name'>
                        OMNI
                    </h1>
                    <p className='text-center text-secondary'>
                        GREAT GAMES FOR GREAT DEALS
                    </p>
                </Jumbotron>
                
    )
}