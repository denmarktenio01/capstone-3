import React, { useContext } from 'react'
import { Button, Jumbotron } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'

import Welcome from '../components/Welcome'
import UserContext from '../UserContext'




export default function Home() {

    const {user} = useContext(UserContext)

    return (    
        <>
            <Welcome></Welcome>
            <div className='text-center'>
                <h3 className='text-warning'>{user.accessToken !== undefined ? `Hello!` : `Hello ${user.firstName}!`}</h3>
                <p className='text-light h4'>
                    Welcome to OMNI! Here at OMNI, we aim to give you the wide selection of great videogames, all in one-click distance! 
                    Role-playing, first-person shooters, online, and single-player games, you name it, we have it!
                </p>
                <Button as={NavLink} to="/shop" className='my-5 p-2'>Browse Now!</Button>
            </div>
            
            
        </>
    )
}