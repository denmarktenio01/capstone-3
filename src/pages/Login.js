import React, {useState, useEffect, useContext} from 'react'
import {Form,Button} from 'react-bootstrap'
import { Redirect, useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login() {
    const history = useHistory()

    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [loginBtn, setLoginBtn] = useState(false)

    useEffect(() => {
        if (email !== "" && password !== ""){
            setLoginBtn(true)
        } else {
            setLoginBtn(false)
        }
    }, [email,password])

    function loginUser(e) {
        e.preventDefault()

        
        fetch('https://quiet-waters-16548.herokuapp.com/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password  
            })
        })
        .then((response) => response.json())
        .then(data => {
            console.log(data)
            
            if (data.accessToken !== undefined) {
                localStorage.setItem('accessToken', data.accessToken);
				setUser({ accessToken: data.accessToken });
                console.log(user)

                Swal.fire({
                    title: 'Yay!',
                    icon: 'success',
                    text: `You're logged in!`
                });

                // get user's details from token
                fetch('https://quiet-waters-16548.herokuapp.com/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    // we then check if the user is admin
                    // if admin, redirect to the /courses page
                    // if not, redirect to homepage
                    
                    localStorage.setItem('isAdmin',data.isAdmin)   
                    localStorage.setItem('firstName',data.firstName)
                    localStorage.setItem('lastName',data.lastName)
                    localStorage.setItem('mobileNumber',data.mobileNumber)
                    localStorage.setItem('email',data.email)
                    setUser({
                        isAdmin: data.isAdmin,
                        firstName: data.firstName,
                        lastName: data.lastName,
                        email: data.email,
                        mobileNumber: data.mobileNumber                        
                    })
                    if (user.isAdmin) {
                        history.push('/productsAdmin')
                    } else {
                        history.push('/')
                    }
                })
            } else if(data.message !== undefined) {
                Swal.fire({
                    title: 'Oops!',
                    icon: 'error',
                    text: `${data.message}`
                });
            } else {
                Swal.fire({
                    title: 'Oops!',
                    icon: 'error',
                    text: `${data.message}`
                })
            }
            setEmail('')
            setPassword('')
        })
    }
    // let's update again the login.js component to make it redirect to homepage when detected that there is a user logged in
    if(user.accessToken !== null) {
        return <Redirect to='/'></Redirect> //
    }


    return (
        <Form className='my-5 pt-5 text-light' onSubmit={(e) => {loginUser(e)}}>
            <Form.Group controlId='userEmail'>
                <Form.Label>Email Address:</Form.Label>
                <Form.Control type='email' placeholder='Enter email' value = {email} onChange={e => setEmail(e.target.value)} required></Form.Control>
                <Form.Text className='text-muted'>
                </Form.Text>
            </Form.Group>

            <Form.Group controlId='password'>
                <Form.Label>Password:</Form.Label>
                <Form.Control type='password' placeholder='Enter password' value={password} onChange={e => setPassword(e.target.value)} required></Form.Control>
            </Form.Group>

            <Button variant='success' className='mb-3 mt-3' type='submit' disabled={!loginBtn}>Login</Button>
        </Form>
    )


}