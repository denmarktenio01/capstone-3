import { Button } from "react-bootstrap";
import React from "react";
import Swal from "sweetalert2";
import { Link, NavLink } from 'react-router-dom';

export default function NotFound() {

    Swal.fire({
            icon: 'error',
            title: "Oops!",
            text: "Page Not Found!"
        })


    return (

        
        <div className='text-center'>
            <h1 className='text-danger'>
                404 Error
            </h1>
            <Button as={NavLink} to='/'>
                Go back to home page.
            </Button>
        </div>
        


    )
}

    

    
