import React, { useContext, useEffect, useState } from 'react'
import { Button, Table, Card, CardDeck, Col, Row, Container } from 'react-bootstrap'

import UserContext from '../UserContext'
import { useHistory } from 'react-router-dom'
import Product from '../components/Product'
import { Fragment } from 'react'
import Swal from 'sweetalert2'
import RemoveOrderBtn from '../components/RemoveOrderBtn'

export default function Orders() {
    const {user} = useContext(UserContext)

    const [userOrders, setUserOrders] = useState([])
    const [overallPrice,setOverallPrice] = useState(0)


    useEffect(() => {
        fetch('https://quiet-waters-16548.herokuapp.com/users/myOrders', {
            headers: {
                Authorization: `Bearer ${localStorage.accessToken}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.message !== undefined){
                return Swal.fire({
                    title: "Oops!",
                    icon: "info",
                    text: `${data.message}`
                })
            }
            setOverallPrice(data.map(order => order.totalAmount.totalPrice).reduce((prev,next) => prev+next))
            setUserOrders(data.map(order => {
                
                return (
                    <tr key={order._id}>
                            <td className='text-center'>{order.productName}</td>
                            <td className='text-center'>{order.totalAmount.quantity}</td>
                            <td className='text-center'>Php {order.totalAmount.totalPrice}</td>
                            <td className='text-center'>
                                <RemoveOrderBtn orderId={order._id}></RemoveOrderBtn>
                            </td>
                    </tr>
                )
            }))

        })
    },[])






    return (
        <>
            <h1 className='text-center pt-5 my-5 text-light'>My Orders</h1>
            <Table className='text-light bg-secondary' striped bordered hover>
                <thead>
                    <tr>
                        <th className='text-center'>Product</th>
                        <th className='text-center'>Quantity</th>
                        <th className='text-center'>Price</th>
                        <th className='text-center'>Actions</th>
                    </tr>
                </thead>
                <tbody>{userOrders}</tbody>
                <tfoot>
                    <tr>
                        <th className='text-center'></th>
                        <th className='text-center'></th>
                        <th className='text-center'>Total bill: Php {overallPrice}</th>
                    </tr>
                </tfoot>
            </Table>
            {/* <h4 className='mr-auto'>{overallPrice}</h4> */}
        </>
    )




}