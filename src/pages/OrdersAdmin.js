import React, { useContext, useEffect, useState } from 'react'
import { Button, Table, Card, CardDeck, Col, Row, Container, Nav, Collapse } from 'react-bootstrap'

import UserContext from '../UserContext'
import { Link, NavLink, useHistory, Redirect } from 'react-router-dom'
import Product from '../components/Product'
import { Fragment } from 'react'
import ViewOrders from '../components/ViewOrders'

export default function OrdersAdmin() {
    const {user,setUser} = useContext(UserContext)

    const [userOrders, setUserOrders] = useState([])
    const [allOrders, setAllOrders] = useState([])
    const [overallPrice,setOverallPrice] = useState(0)
    




    useEffect(() => {
        fetch('https://quiet-waters-16548.herokuapp.com/users/orders', {
            headers: {Authorization: `Bearer ${localStorage.accessToken}`}
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setAllOrders(data.map(customer => {
                return (
                    <tr key={customer._id}>
                            <td className='text-center'>{customer.email}</td>
                            <td className='text-center'>{customer.orders.length}</td>
                            <td className='text-center'><ViewOrders user={customer}></ViewOrders></td>
                            {/* <td className='d-flex justify-content-around'>
                            </td> */}
                    </tr>
                )
            }))
        })
    }, [])


    if(!user.isAdmin){
        return <Redirect to='/naughty'></Redirect>
    }

    






    return (
        <>
        
            <h1 className='text-center my-5 text-light'>Orders</h1>
            <Table className='text-light bg-secondary' striped bordered hover>
                <thead className='text-white'>
                    <tr>
                        <th className='text-center'>User Email</th>
                        <th className='text-center'>User Orders</th>
                        <th className='text-center'>Action</th>
                    </tr>
                </thead>
                <tbody>{allOrders}</tbody>
                {/* <tfoot>
                    <tr>
                        <th className='text-center'></th>
                        <th className='text-center'></th>
                        <th className='text-center'>Total bill: Php {overallPrice}</th>
                    </tr>
                </tfoot> */}
            </Table>
            {/* <h4 className='mr-auto'>{overallPrice}</h4> */}
        </>
    )
}