import React, { useContext, useEffect, useState } from 'react'
import { Button, Table, Card, CardDeck, Col, Row, Container } from 'react-bootstrap'

import UserContext from '../UserContext'
import { useHistory } from 'react-router-dom'
import Product from '../components/Product'
import { Fragment } from 'react'
import UpdateProductBtn from '../components/UpdateProductBtn'
import AddProductBtn from '../components/AddProductBtn'
import ArchiveBtn from '../components/ArchiveBtn'

export default function Products() {
    // const [searchTerm, setSearchTerm] = useState("")

    // const [filterProducts, setFilterProducts] = useState([])
    const [allProducts, setAllProducts] = useState([])
    const history = useHistory()

    const { user } = useContext(UserContext);
    console.log(user)

    


    useEffect(() => {
            fetch('https://quiet-waters-16548.herokuapp.com/products/', {
                headers: {
                    Authorization: `Bearer ${localStorage.accessToken}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                setAllProducts(data.map(product => {
                    return (
                            <Product key={product._id} products={product} productId={product._id}></Product>  
                    )
                }))

            })
        }, [])
        
                
        
        

    







    


    return (
        <>
            
            <div className='text-center pt-5 text-light'>
                <h1 className='text-center mt-5 '>Products in Offer</h1>
                {/* <input className='h3' type='text' value={searchTerm} onChange={e => setSearchTerm(e.target.value)}></input> */}
            </div>
            

                    <Container className='my-2' > 
                        
                        <CardDeck id='products' className='d-flex justify-content-center'>
                            {allProducts}
                        </CardDeck>
                    </Container>
                    
            
        </>
    )



















}