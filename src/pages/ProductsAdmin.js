import React, { useContext, useEffect, useState } from 'react'
import { Button, Table, Card, CardDeck, Col, Row, Container } from 'react-bootstrap'

import UserContext from '../UserContext'
import { Redirect, useHistory } from 'react-router-dom'
import Product from '../components/Product'
import { Fragment } from 'react'
import UpdateProductBtn from '../components/UpdateProductBtn'
import AddProductBtn from '../components/AddProductBtn'
import ArchiveBtn from '../components/ArchiveBtn'
import RestoreBtn from '../components/RestoreBtn'


export default function ProductsAdmin() {
    const [ping, setPing] = useState(false)
    const {user, setUser, unsetUser} = useContext(UserContext)
    const [adminProducts, setAdminProducts] = useState([])


    


    const history = useHistory()

    

    useEffect(() => {
        
        fetch('https://quiet-waters-16548.herokuapp.com/products/all', {
                headers: {
                    Authorization: `Bearer ${localStorage.accessToken}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if(data.message){
                    history.push('/naughty')
                }
                    setAdminProducts(data.map(product => {
                    // render react elements into our components

                    return (
                        

                        <tr key={product._id}>
                            <td>{product._id}</td>
                            <td>{product.name}</td>
                            <td>{product.price}</td>
                            <td className={product.isActive ? 'text-success' : 'text-danger'}  >{product.isActive ? "Active" : "Inactive"}</td>
                            <td className='d-flex justify-content-around'>
                                <UpdateProductBtn productId={product._id}></UpdateProductBtn>
                                {product.isActive ? <ArchiveBtn productId={product._id} /> : <RestoreBtn productId={product._id} />}
                            </td>
                            
                        </tr>
                    )
                }))
                // }
                
            })
    }, [])

    if(user.isAdmin === false) {
        return <Redirect to='/naughty'></Redirect> //
    }
    









    return (
        <>
            <h1 className='text-center my-5 text-light'>Products Dashboard</h1>
            <Table className='text-light bg-secondary' striped bordered hover>
                <thead>
                    <tr>
                        <th className='text-center'>ID</th>
                        <th className='text-center'>Name</th>
                        <th className='text-center'>Price</th>
                        <th className='text-center'>Status</th>
                        <th className='text-center'>Actions</th>
                    </tr>
                </thead>
                <tbody>{adminProducts}</tbody>
            </Table>
            <div className='mb-5'>
                <AddProductBtn></AddProductBtn>
            </div>
            
        </>
    )
}