import React, {useState, useEffect, useContext} from 'react'
import {Form,Button} from 'react-bootstrap'
import { Redirect, useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Register () {
    const history = useHistory()
    const {user} = useContext(UserContext)

    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [email, setEmail] = useState("")
    const [mobileNumber, setMobileNumber] = useState("")
    const [password, setPassword] = useState("")
    const [verifyPassword, setVerifyPassword] = useState("")
    const [registerBtn, setRegisterBtn] = useState(false) 



    useEffect(() => {
        if(email !== '' && password !== '' && verifyPassword !== '' && mobileNumber.length === 11){
            setRegisterBtn(true)
        } else {
            setRegisterBtn(false)
        }
    }, [email, password, verifyPassword, mobileNumber])



    function registerUser(e) {
        e.preventDefault()

        if(password !== verifyPassword) {
            Swal.fire({
                title: 'Oops!',
                icon: 'error',
                text: `Passwords don't match!`
            })
        } else {
            fetch('https://quiet-waters-16548.herokuapp.com/users/register', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    mobileNumber: mobileNumber,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                // if(data.message !== null) {
                //     Swal.fire({
                //         title: 'Oops!',
                //         icon: 'error',
                //         text: `${data.message}`
                //     })
                // }

                if(data === true){
                    Swal.fire({
                        title: 'Thank you!',
                        icon: 'success',
                        text: 'You have successfuly registered!'
                    })
    
                    history.push('/login')
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: `${data.message}`
                    })
                }
            })
        }

        setFirstName('')
        setLastName('')
        setEmail('')
        setMobileNumber('')
        setPassword('')
        setVerifyPassword('')

        
    }
    // for redirection to home if there is a logged in user
    if(user.accessToken !== null){
        return <Redirect to='/'></Redirect>
    }




    return(
        <Form className='my-5 pt-5 text-light' onSubmit={(e) => {registerUser(e)}}>
            <Form.Group controlId='firstName'>
                <Form.Label>First Name:</Form.Label>
                <Form.Control type='text' placeholder='Enter first name' value = {firstName} onChange={e => setFirstName(e.target.value)} required></Form.Control>
            </Form.Group>

            <Form.Group controlId='lastName'>
                <Form.Label>Last Name:</Form.Label>
                <Form.Control type='text' placeholder='Enter last name' value = {lastName} onChange={e => setLastName(e.target.value)} required></Form.Control>
            </Form.Group>



            <Form.Group controlId='userEmail'>
                <Form.Label>Email Address:</Form.Label>
                <Form.Control type='email' placeholder='Enter email' value = {email} onChange={e => setEmail(e.target.value)} required></Form.Control>
                <Form.Text className='text-muted'>
                    Your email is safe with us.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId='mobileNumber'>
                <Form.Label>Mobile Number:</Form.Label>
                <Form.Control type='number' placeholder='Enter mobile number' value = {mobileNumber} onChange={e => setMobileNumber(e.target.value)} required></Form.Control>
            </Form.Group>

            <Form.Group controlId='password1'>
                <Form.Label>Password:</Form.Label>
                <Form.Control type='password' placeholder='Enter password' value={password} onChange={e => setPassword(e.target.value)} required></Form.Control>
            </Form.Group>

            <Form.Group controlId='password2'>
                <Form.Label>Confirm Password:</Form.Label>
                <Form.Control type='password' placeholder='Verify password' value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)} required></Form.Control>
            </Form.Group>
            <Button variant='dark' className='mb-3' type='submit' disabled={!registerBtn}>Submit</Button>
        </Form>
    )




}

