import React, { useContext, useEffect, useState } from 'react'
import { Button, Collapse, Form,  } from 'react-bootstrap'
import ChangePasswordBtn from '../components/ChangePasswordBtn'
import EditInfo from '../components/EditInfo'
import UserContext from '../UserContext'









export default function UserAccount() {

    

















    return (
        <div className='text-center pt-5 mt-5'>
        <h1 className='text-primary text-center  my-5'>Account Information</h1>
        <h3 className='text-secondary my-5'>Personal Information</h3>
        <div className='text-light my-5'>
            <h4>Email: {localStorage.email}</h4>
            <h4>First Name: {localStorage.firstName}</h4>
            <h4>Last Name: {localStorage.lastName}</h4>
            
            <h4>Mobile Number: {localStorage.mobileNumber}</h4>
        </div>
        <div>
            <EditInfo></EditInfo>
            <ChangePasswordBtn />
        </div>

        </div>
    )
}